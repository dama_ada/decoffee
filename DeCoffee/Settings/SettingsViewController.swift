//
//  SettingsViewController.swift
//  DeCoffee
//
//  Created by alfredo pinfildi on 14/10/2018.
//  Copyright © 2018 DAMA. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    let cellLabelText = ["Quantity", "Cost"]
    let sectionHeaderText = ["Setup your daily quantity and cost", "Audio setup"]
    let sectionNumberRows = [2, 1]
    let textColor = UIColor(red: CGFloat(56.0/255.0), green: CGFloat(34.0/255.0), blue: CGFloat(15.0/255.0), alpha: CGFloat(1.0))
    var user = User.userInstance
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var rightButtonNavigationBar: UIBarButtonItem!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.isScrollEnabled = false
        
        if user.cost == 0.0 || user.quantity == 0 {
            rightButtonNavigationBar.isEnabled = false
        }
        
    }

    
    @IBAction func done(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return sectionNumberRows[section]
   
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
        let label = UILabel()
        label.text = sectionHeaderText[section]
        let color = UIColor(red: CGFloat(236.0/255.0), green: CGFloat(224.0/255.0), blue: CGFloat(209.0/255.0), alpha: CGFloat(1))
        label.backgroundColor = color
        label.textAlignment = .center
        label.textColor = textColor
        return label
    
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return getCell(indexPath: indexPath)
    
    }
  
    
    func getCell(indexPath: IndexPath) -> UITableViewCell {
    
        var cell = UITableViewCell()
        switch indexPath.section {
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell")!
            let label = cell.viewWithTag(1) as? UILabel
            let input = cell.viewWithTag(2) as? UITextField
            
            switch indexPath.row {
                case 0:
                    input?.text = String(User.userInstance.quantity)
                case 1:
                    input?.text = String(User.userInstance.cost)
                default:
                    break
                
            }
            
            input?.delegate = self
            
            label?.textColor = textColor
            input?.textColor = textColor
            
            label?.text = cellLabelText[indexPath.row]
            //IMPLEMENTAZIONE DI DATAMANAGER NECESSARIA
            //input?.text = DataManager.coffeeData[indexPath.row]
            
            return cell
        
        case 1:
            cell = tableView.dequeueReusableCell(withIdentifier: "audioCell")!
            let label = cell.viewWithTag(1) as? UILabel
            let audioSwitch = cell.viewWithTag(2) as? UISwitch
        
            label?.textColor = textColor
            
            label?.text = "Audio"
            
            return cell
            
        default:
            return cell
        }
    
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let cell = textField.superview!.superview as? UITableViewCell
        let index = tableView.indexPath(for: cell!)?.row
        
        switch index {
        case 0:
            User.userInstance.quantity = Int(textField.text!)!
        case 1:
            User.userInstance.cost = Double(textField.text!)!
            rightButtonNavigationBar.isEnabled = true
        default:
            break
        }
        //IMPLEMENTAZIONE DI DATAMANAGER NECESSARIA
        //DataManager.coffeeData[indexPath.row] = textField.text
        return true
    
    }
    
    
    
    
    
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
