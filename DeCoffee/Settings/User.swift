//
//  User.swift
//  DeCoffee
//
//  Created by alfredo pinfildi on 16/10/2018.
//  Copyright © 2018 DAMA. All rights reserved.
//

import UIKit

class User {
    
    static let userInstance = User()
    var currentDate: String {
        
        get {
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yyyy"
            return formatter.string(from: date)
            
        }
        
    }
    
    var quantity: Int {
        
        get {
            return UserDefaults.standard.integer(forKey: "quantity")
        }
        
        set (quantity) {
            UserDefaults.standard.set(quantity, forKey: "quantity")
        }
        
    }
    
    var cost: Double {
        
        get {
            return UserDefaults.standard.double(forKey: "cost")
        }
        
        set (cost) {
            UserDefaults.standard.set(cost, forKey: "cost")
        }
        
    }
    
    var dailyQuantity: Int {
        
        get {
            return UserDefaults.standard.integer(forKey: currentDate + "Quantity")
        }
        
        set (dailyQuantity) {
            UserDefaults.standard.set(dailyQuantity, forKey: currentDate + "Quantity")
        }
    }
    
    
    private init() {}
    
}
