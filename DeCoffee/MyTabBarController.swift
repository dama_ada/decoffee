//
//  MyTabViewController.swift
//  DeCoffee
//
//  Created by Mattia Fusco on 19/10/2018.
//  Copyright © 2018 DAMA. All rights reserved.
//

import UIKit

class MyTabBarController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        
        // GESTURE RECOGNIZER PER SWIPE A SINISTRA
        let gestureLeft = UISwipeGestureRecognizer(target: self, action: #selector(swiped))
        gestureLeft.direction = .left
        self.view.addGestureRecognizer(gestureLeft)
        
        // GESTURE RECOGNIZER PER SWIPE A DESTRA
        let gestureRight = UISwipeGestureRecognizer(target: self, action: #selector(swiped))
        gestureRight.direction = .right
        self.view.addGestureRecognizer(gestureRight)
    }
    
    // FUNZIONE DEL DELEGATO PER TRANSIZIONE CUSTOM
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        guard let fromView = selectedViewController?.view, let toView = viewController.view else {
            return false // Make sure you want this as false
        }
        
        if fromView != toView {
            UIView.transition(from: fromView, to: toView, duration: 0.5, options: [.transitionCrossDissolve], completion: nil)
        }
        self.selectedIndex = viewControllers!.lastIndex(of: viewController)!
        return true
    }
    
    // CALLBACK DEL GESTURE RECOGNIZER
    @objc func swiped(_ gestureRecognizer: UISwipeGestureRecognizer){
        print("swiped")
        if gestureRecognizer.direction == .right {
            print("swiped right")
            if self.viewControllers?.firstIndex(of: selectedViewController!) == 0 {
                return
            } else {
                
                tabBarController(self, shouldSelect: viewControllers![0])
               
            }
        }
        if gestureRecognizer.direction == .left {
            print("swiped left")
            if self.viewControllers?.firstIndex(of: selectedViewController!) == 1 {
                return
            } else {
                
                tabBarController(self, shouldSelect: viewControllers![1])
                
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
