//
//  HomeViewController.swift
//  DeCoffee
//
//  Created by Daniele Sesto on 16/10/2018.
//  Copyright © 2018 DAMA. All rights reserved.
//

import UIKit
import AVFoundation

class HomeViewController: UIViewController {
    
    @IBOutlet weak var spent: UILabel!
    @IBOutlet weak var today: UILabel!
    var imageViews = [UIImageView]()
    var counter = 1
    @IBOutlet var myView: UIView!
    var user = User.userInstance
    var musicEffect: AVAudioPlayer = AVAudioPlayer()
    var currentDate: String {
        
        get {
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yyyy"
            return formatter.string(from: date)
            
        }
        
    }
    var expenses: [String: Double] {
        
        get {
            
            guard let expensesStore = UserDefaults.standard.dictionary(forKey: "expenses") else {
                return [:]
            }
            
            return expensesStore as! [String: Double]
            
        }
        
    }

    
    override func viewDidAppear(_ animated: Bool) {
        
        if !checkUserSettings() {
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Settings", bundle: nil)
            let settingsViewController = storyBoard.instantiateViewController(withIdentifier: "SettingsController") as! SettingsViewController
            self.present(settingsViewController, animated: true, completion: nil)
            
        } else {
            
            let asset = NSDataAsset(name: "myAsset")
            let myDataAsset = NSDataAsset(name: "coffeePic")
            var imageView : UIImageView?
            var temp = 0
            print(user.dailyQuantity)
            let tot: Int = Int(myView.bounds.width)
            let left = (tot - (31 * user.quantity)) / 2
            
            for _ in 1...user.quantity {
                
                imageView = UIImageView(frame: CGRect(x: left + temp, y: 420, width: 56, height: 56))
                imageView?.image = UIImage(data:(asset?.data)!)
                
                myView.addSubview(imageView!)
                imageViews.append(imageView!)
                
                temp = temp + 25
                
            }
            
            if (user.dailyQuantity > 0) {
        
                for i in 1...user.dailyQuantity {
                
                    //imageViews[imageViews.count - i].image = UIImage(data: (myDataAsset?.data)!)
                
                }
            
            }
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.spent.text = "you spent \(user.cost * Double(user.dailyQuantity)) €"
        self.today.text = "you saved \(user.cost * (Double(user.quantity) - Double(user.dailyQuantity))) €"
        
    }
    
    
    @IBAction func coinToCoffee(_ sender: Any) {
        let myDataAsset = NSDataAsset(name: "coffeePic")
        
        addExpense(expenseValue: user.cost)
        
        if imageViews.count - counter >= 0{
            imageViews[imageViews.count - user.dailyQuantity].image = UIImage(data: (myDataAsset?.data)!)
            counter = counter + 1
        }
        
        self.spent.text = "you spent \(user.cost * Double(user.dailyQuantity)) €"
        self.today.text = "you saved \(user.cost * (Double(user.quantity) - Double(user.dailyQuantity))) €"
        
    }
    
    
    func addExpense(expenseValue: Double) {
        
        var expenses: [String: Double]
        
        if user.dailyQuantity < user.quantity {
            
            guard let expensesStored = UserDefaults.standard.dictionary(forKey: "expenses") else {
                
                expenses = [currentDate: expenseValue]
                UserDefaults.standard.set(expenses, forKey: "expenses")
                user.dailyQuantity = 1
                
                return
                
            }
            
            expenses = expensesStored as! [String : Double]
            guard let todayExpensesAmount = expenses[currentDate] else {
                
                expenses[currentDate] = user.cost
                UserDefaults.standard.set(expenses, forKey: "expenses")
                user.dailyQuantity = 1
                
                return
                
            }
            
            expenses.updateValue(todayExpensesAmount + user.cost, forKey: currentDate)
            UserDefaults.standard.set(expenses, forKey: "expenses")
            user.dailyQuantity += 1
            
        }
        
    }
    
    
    func checkUserSettings() -> Bool {
        
        if (user.cost == 0.0 || user.quantity == 0) {
            
            return false
            
        } else {
            
            return true
            
        }
        
    }
    
}


/*
 let asset = NSDataAsset(name: "CoinSoundEffect")
 do {
 
 musicEffect = try AVAudioPlayer(data: (asset?.data)!, fileTypeHint: "mp3")
 
 }
 
 catch {
 print(error)
 }
 */

/*@IBAction func playsound(_ sender: Any) {
 
 
 musicEffect.play()
 
 }*/
