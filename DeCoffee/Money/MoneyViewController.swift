//
//  MoneyViewController.swift
//  DeCoffee
//
//  Created by Mattia Fusco on 15/10/2018.
//  Copyright © 2018 DAMA. All rights reserved.
//

import UIKit
import Charts

class MoneyViewController: UIViewController {
    
    @IBOutlet var view1: BarChartView!
    
    @IBOutlet var view2: LineChartView!
    
    @IBOutlet var label1: UILabel!
    @IBOutlet var label2: UILabel!
    @IBOutlet var label3: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBAction func segmentedAction(_ sender: Any) {
        print(segmentedControl.selectedSegmentIndex)
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            view1.isHidden = true
            view2.isHidden = false
            
            label1.isHidden = false
            label2.isHidden = false
            label3.isHidden = true
        case 1:
            view1.isHidden = false
            view2.isHidden = true
            label1.isHidden = true
            label2.isHidden = true
            label3.isHidden = false

        default:
            return
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedControl.selectedSegmentIndex = 0
        updateBarChart()
        updateLineChart()
        view1.isHidden = true
        view2.isHidden = false
        label1.isHidden = false
        label2.isHidden = false
        label3.isHidden = true
        
        
        // Do any additional setup after loading the view.
    }
    
    func updateBarChart(){
        var entries = [BarChartDataEntry(x: 0, y: 2),
                       BarChartDataEntry(x: 1, y: 4),
                       BarChartDataEntry(x: 2, y: 6),
                       BarChartDataEntry(x: 3, y: 8)]
        
        var dataSet = BarChartDataSet(values: entries, label: "Savings per week")
        dataSet.colors = [NSUIColor(red: CGFloat(99.0/255.0), green: CGFloat(72.0/255.0), blue: CGFloat(50.0/255.0) , alpha: CGFloat(1))]
        self.view1.xAxis.granularity = 1.0
        self.view1.xAxis.valueFormatter = MyWeekFormatter()
        self.view1.data = BarChartData(dataSet: dataSet)
        
        
    }
    
    func updateLineChart() {
        var xAxis = view2.xAxis
        xAxis.valueFormatter = MyValueFormatter()
        var entries = [ChartDataEntry(x: 1, y: 10),
                       ChartDataEntry(x: 2, y: 3),
                       ChartDataEntry(x: 3, y: 7),
                       ChartDataEntry(x: 4, y: 9),
                       ChartDataEntry(x: 5, y: 4),
                       ChartDataEntry(x: 6, y: 6),
                       ChartDataEntry(x: 7, y: 10)
        ]
        var dataSet = LineChartDataSet(values: entries, label: "Coffee per day")
        dataSet.circleColors = [NSUIColor(red: CGFloat(99.0/255.0), green: CGFloat(72.0/255.0), blue: CGFloat(50.0/255.0) , alpha: CGFloat(1))]
        dataSet.colors = [NSUIColor(red: CGFloat(99.0/255.0), green: CGFloat(72.0/255.0), blue: CGFloat(50.0/255.0) , alpha: CGFloat(1))]
        
        self.view2.data = LineChartData(dataSet: dataSet)
        
    }
    
    
}


class MyValueFormatter : IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        print(axis)
        switch value {
            case 1...1.99:
                return "Mon"
            case 2...2.99:
                return "Tue"
            case 3...3.99:
                return "Wed"
            case 4...4.99:
                return "Thu"
            case 5...5.99:
                return "Fri"
            case 6...6.99:
                return "Sat"
            case 7...7.99:
                return "Sun"
            default:
                return ""
        }
    }
    
    
}

class MyWeekFormatter : IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        switch value {
        case 0:
            return "Week1"
        case 1:
            return "Week 2"
        case 2:
            return "Week 3"
        case 3:
            return "Week 4"
        default:
            return ""
        }
    }
    
    
}
