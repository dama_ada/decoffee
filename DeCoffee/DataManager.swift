//
//  DataManager.swift
//  DeCoffee
//
//  Created by Mattia Fusco on 15/10/2018.
//  Copyright © 2018 DAMA. All rights reserved.
//

import Foundation
import CoreData
import UIKit
class DataManager {
    
    private var context : NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let container = appDelegate?.persistentContainer
        return (container?.viewContext)!
    }
    
    func addItem(){
        
    }
    func removeItem(){
        
    }
    
    func fetchItems(){
        
    }
        
}
